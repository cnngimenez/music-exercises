Music Exercises

Some music information and exercises.

This source code repository can generate the following contents:

- PDF Score files.
- MIDI files.
- OGG files.
- A PDF complete documentation with references.

Once generated, they will be available at the `./produced/` directory. 

# Requirements
To generate them, it is required the following software packages:

- Lilypond (to create PDF scores).
- Timidity (to create playable MIDI and OGG files).
- GNU/Linux Make utilities (`make` program).

## Timidity
To play MIDI and to create OGG files Timidity and/or Timidity++ requires soundfonts. Install soundfont-fluid or freepats-general-midi packages into your system. Also, configure Timidity to use them. If no soudfont appears inside the `/etc/timidity/timidity.cfg` file, add the following line:

```
soundfont /usr/share/soundfonts/freepats-general-midi.sf2
```

# Compiling
Type `make`at the main directory of this project. Also, the `make clean; make` command can be used to remove and compile everything from zero. This is useful if your file just does not compile as expected.

# License
See each file header and contents for information about their respective licenses.

Generaly the following licenses are used to support the free (as in freedom) culture: CC-By-SA for artistic and reading contents, and General Public License v3 for scripts and programs.

## Lilypond, PDF scores, MIDI and OGG files
<https://i.creativecommons.org/l/by-sa/4.0/88x31.png>

Lilypond, PDF scores, MIDI and OGG files in this work are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

See the following page for more information: http://creativecommons.org/licenses/by-sa/4.0/

## Scripts, Makefiles and other programs
<https://www.gnu.org/graphics/gplv3-127x51.png>

Scripts, program source code, Makefiles files in this work are under the GNU General Public License version 3 (GPLv3).

See the following page for more information: https://www.gnu.org/licenses/gpl-3.0.html
