# Copyright 2023 Christian Gimenez

# Author: Christian Gimenez   

# Makefile-ogg.make

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

MIDI_FILENAMES := $(notdir $(wildcard produced/midi/*.midi))
OGG_FILENAMES := $(addsuffix .ogg, $(basename $(MIDI_FILENAMES)))
OGG_PATHS := $(addprefix produced/ogg/, $(OGG_FILENAMES))

produced/ogg/%.ogg: produced/midi/%.midi
	echo "Compiling $< into $@"
	timidity -Ov -o $@ $<

.PHONY: test ogg all
.SILENT: test


all: ogg

ogg: $(OGG_PATHS)

test:
	echo "- MIDI found:"
	echo $(MIDI_FILENAMES)
	echo "- Ogg to compile"
	echo $(OGG_FILENAMES)
	echo "Paths:"
	echo $(OGG_PATHS)
