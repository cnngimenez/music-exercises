%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% Inteded compiler: LuaLaTeX

\documentclass[12pt,a4paper,draft]{book}

%% \usepackage{polyglossia}
%% \setmainlanguage{english}

\usepackage{lilyglyphs}

\usepackage{ifdraft}

\usepackage[colorinlistoftodos, shadow, loadshadowlibrary, obeyDraft,
textsize=tiny, textwidth=2cm, spanish, bordercolor=red!90,
linecolor=red!50, backgroundcolor=red!20,]{todonotes}

\usepackage{emoji}
\usepackage{color}
\usepackage{soul} %% \hl and highlighting

%% Include graphics and images.
\PassOptionsToPackage{final}{graphicx}
\setkeys{Gin}{draft=false}
\usepackage{graphicx} %% Show images even in draft-mode.
%% \graphicspath{{imgs}{.}}
\usepackage{wrapfig}

\definecolor{deepskyblue}{HTML}{00bfff}
\definecolor{springgreen}{HTML}{00fa9a}
\definecolor{turquoise}{HTML}{40e0d0}
\definecolor{darkturquoise}{HTML}{00ced1}
\definecolor{seagreen}{HTML}{2e8b57}
\definecolor{darkcyan}{HTML}{008b8b}
\usepackage[final,colorlinks]{hyperref}
%% PDF information
\hypersetup{
  pdfauthor={Christian Gimenez},
  pdftitle={Music Exercises},
  pdfkeywords={music},
  pdfsubject={},
  pdfcreator={Lilypond and LaTeX}, 
  pdflang={English}
  ,linkcolor=darkcyan
  ,citecolor=darkcyan
  ,urlcolor=deepskyblue
} 
\usepackage[capitalise]{cleveref}
\crefname{score}{Score}{Scores}
\usepackage{titling}

%% Watermarks and line numbers to easy reference for draft-mode.
%% Warning: Watermarks are quite problematic for text selection.
\ifdraft{
  \usepackage[modulo]{lineno} 
  %% \usepackage{draftwatermark}
}{}

%% We use a more comfortable font, but similar to the usual Latin Modern.
%% See: https://tug.org/FontCatalogue/mlmodern/
\usepackage{fontenc}
\usepackage[T1]{fontenc}
\usepackage{mlmodern}

%% BibLaTeX to make citations and references
%% \usepackage{csquotes} 
\usepackage[backend=biber, style=alphabetic,
maxnames=10, minnames=10,
backref=true]{biblatex}
\addbibresource{../biblio.bib}

%% Code listing. This is very interesting and useful to show code.
\usepackage[final]{listings}

%% --------------------------------------------------
%% Float environments
%% --------------------------------------------------

%% Defines float environments.
\usepackage{float}

%% This is the float environment for music scores.
\newfloat{score}{tbp}{lop}[section]
\floatname{score}{Score}
\def\lstfloatautorefname{Score}

%% This is a float environment for source code.
\newfloat{listfloat}{tbp}{lop}[section]
\floatname{listfloat}{Listing}
\def\lstfloatautorefname{Listing}

\usepackage{makeidx} \makeindex

\input{commands.lytex}

\title{Music Reference and Exercises}
\author{Christian Gimenez}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\frontmatter
\phantomsection
\addcontentsline{toc}{chapter}{Cover}
%% \maketitle
\input{cover.tex}

%% Watermark and line-numbering instructions.
%% Use it after cover!
\ifdraft{ 
    %%\DraftwatermarkOptions{stamp=true,text={\textrm{DRAFT}}}
    \linenumbers
}{}
\ifdraft{\Huge \emoji{writing-hand} \hl{This is a draft writing!} }{}

\newpage
\phantomsection
\addcontentsline{toc}{chapter}{License}

\large \textbf{ License:} \normalsize
\vspace*{1cm}

\begin{center}
  \includegraphics[width=2.5cm]{../imgs/by-sa.png}

  This work is licensed under a
  
  Creative Commons - Attribution - ShareAlike 4.0 International License

  (CC-by-SA 4.0 International).
\end{center}

Visit the following URL for more information:
\begin{center}
    \url{http://creativecommons.org/licenses/by-sa/4.0/}
\end{center}

\vspace*{1cm}

\phantomsection
\addcontentsline{toc}{chapter}{Book version}

\large \textbf{Book version:} \normalsize

This book were compiled using Lua\LaTeX{} and Lilypond programs on \today{}. Source code used are available at \homepage{}.

Git were used as distributed version control system. Source code revision used were~\texttt{\commit} on branch~\texttt{\branch}.

\vfill


\phantomsection
\addcontentsline{toc}{chapter}{Table of Contents}
\renewcommand{\contentsname}{Table of Contents}
\tableofcontents

\ifdraft{\listoftodos{}}

\phantomsection
\addcontentsline{toc}{chapter}{List of Scores}
\listof{score}{List of Scores}

%% \phantomsection
%% \addcontentsline{toc}{chapter}{List of Figures}
%% \listoffigures{}

\phantomsection
\addcontentsline{toc}{chapter}{List of Tables}
\listoftables{}


\mainmatter
\input{about-this-book.lytex}
\input{scales.lytex}
\chapter{Voice}
\input{vocal-tessiture.lytex}
\input{chords.lytex}
\input{development.lytex}
\input{license.tex}

\backmatter
\phantomsection
\addcontentsline{toc}{chapter}{References}
\printbibliography[title={References}]

\phantomsection
\addcontentsline{toc}{chapter}{Index}
\label{chap:index}
\printindex

\end{document}
