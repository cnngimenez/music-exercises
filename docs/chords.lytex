\chapter{Chords}
\index{chords}

A \textbf{triad} is a three simultaneous notes\index{triad}. It is a chord which each pitch are related by a specific intervals. Usually, the name is used to refer to the \textbf{tertian} triad\index{tertrian}\index{triad!tertrian}, which each note on the chord are distanced in interval of a third. For example, using the C scale pitchs, their triads are as shown on \cref{score:c-triads}.

The structure of each triad constructed in the scale differs between major and minor thirds on each interval. For instance, the triad constructed on $1^\circ$ degree uses one M3 and then one m3, the triad on $2^\circ$ uses m3 and then M3, and the triad on $7^\circ$ degree uses m3 and m3 intervals. Following this analysis, three types of triads are found on the scale. They are named ``major triads'' for triads with M3 and m3 intervals, ``minor triads'' for m3 and M3 intervals, and ``diminished triad'' for m3 and m3. The \cref{score:c-triads-order} shows the same triads as \cref{score:c-triads} but ordered by their type \cite{blatter07:_revis_music_theor}.
\index{triad!major}\index{triad!minor}\index{triad!diminished}
\index{major triad}\index{minor triad}\index{diminished  triad}

Also, \cref{tab:c-triads-structure} shows the triads notes formed from the C scale, their structure, name, and grades. The distribution of major, minor and diminished through the grades of the scale can be appreciated.

\begin{table}
  \caption{Triads from C scale, their structures and names.}
  \label{tab:c-triads-structure}
  
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
    \hline
    G     &  A     & B     & C     & D     & E     & F     & G     \\
    E     &  F     & G     & A     & B     & C     & D     & E     \\
    C     &  D     & E     & F     & G     & A     & B     & C     \\
    \hline
    M3-m3 &  m3-M3 & m3-M3 & M3-m3 & M3-m3 & m3-M3 & m3-m3 & M3-m3 \\
    Major &  Minor & Minor & Major & Major & Minor & Dim.  & Major \\
    1     & 2      & 3     & 4     & 5     & 6     & 7     & 8/1   \\
    \hline
  \end{tabular}
\end{table}

\begin{score}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <c e g>1_\markup{"1°"}
          <d f a>_\markup{"2°"}
          <e g b>_\markup{"3°"}
          <f a c>_\markup{"4°"}
          <g b d>_\markup{"5°"}
          <a c e>_\markup{"6°"}
          <b d f>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
  
  \caption{Triads constructed with C scale pitchs.}
  \label{score:c-triads}
\end{score}

\begin{score}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <c e g>1_\markup{"1°"}^\markup{Major triads}
          <f a c>_\markup{"4°"}
          <g b d>_\markup{"5°"}
          \bar "||"
          <d f a>_\markup{"2°"}^\markup{Minor triads}
          <e g b>_\markup{"3°"}          
          <a c e>_\markup{"6°"}
          \bar "||"          
          <b d f>_\markup{"7°"}^\markup{Diminished triads}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
  
  \caption{C scale triads orderder by type.}
  \label{score:c-triads-order}
\end{score}


\section{Natural Major Chords}
\index{chords!natural major}

\todo[inline]{Explanation about chords. References!}

\begin{table}
  \caption{Tones and semitones of the natural major chords}
  \label{tab:natur-major-chord}
  \centering
  
  \begin{tabular}{*{8}{|c}|}
    \hline
    C &    & E &       & G &       & C \\
    \hline
    \ & 2T &   & 1\textonehalf{}T &   & 2\textonehalf{}T &   \\
    \hline
    \ & M3 &   & m3    &   & P4    &   \\
    \hline
  \end{tabular}
\end{table}

\begin{lilypond}
  \score {
    <<
    \chords {
      c1 d e f g a b
    }
    \new Staff {
      \relative c' {
        <c e g>1
        <d fis a>
        <e gis b>
        <f a c>
        <g b d>
        <a cis e>
        <b dis fis>
      }
    }
    >>
  }
\end{lilypond}

\subsection{C Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <c e g>1_\markup{"1°"}
          <d f a>_\markup{"2°"}
          <e g b>_\markup{"3°"}
          <f a c>_\markup{"4°"}
          <g b d>_\markup{"5°"}
          <a c e>_\markup{"6°"}
          <b d f>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{D Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <d   fis a  >1_\markup{"1°"}
          <e   g   b  >_\markup{"2°"}
          <fis a   cis>_\markup{"3°"}
          <g   b   d  >_\markup{"4°"}
          <a   cis e  >_\markup{"5°"}
          <b   d   fis>_\markup{"6°"}
          <cis e   g  >_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{D Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <d   fis a  >1_\markup{"1°"}
          <e   g   b  >_\markup{"2°"}
          <fis a   cis>_\markup{"3°"}
          <g   b   d  >_\markup{"4°"}
          <a   cis e  >_\markup{"5°"}
          <b   d   fis>_\markup{"6°"}
          <cis e   g  >_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{E Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <e   gis b  >1_\markup{"1°"}
          <fis a   cis>_\markup{"2°"}
          <gis b   dis>_\markup{"3°"}
          <a   cis e  >_\markup{"4°"}
          <b   dis fis>_\markup{"5°"}
          <cis e   gis>_\markup{"6°"}
          <dis fis a  >_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{F Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <f   a   c  >1_\markup{"1°"}
          <g   bes d  >_\markup{"2°"}
          <a   c   e  >_\markup{"3°"}
          <bes d   f  >_\markup{"4°"}
          <c   e   g  >_\markup{"5°"}
          <d   f   a  >_\markup{"6°"}
          <e   g   bes>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{G Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <g   b   d  >1_\markup{"1°"}
          <a   c   e  >_\markup{"2°"}
          <b   d   fis>_\markup{"3°"}
          <c   e   g  >_\markup{"4°"}
          <d   fis a  >_\markup{"5°"}
          <e   g   b  >_\markup{"6°"}
          <fis a   c  >_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{A Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <a   cis e  >1_\markup{"1°"}
          <b   d   fis>_\markup{"2°"}
          <cis e   gis>_\markup{"3°"}
          <d   fis a  >_\markup{"4°"}
          <e   gis b  >_\markup{"5°"}
          <fis a   cis>_\markup{"6°"}
          <gis b   d  >_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{B Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <b   dis fis>1_\markup{"1°"}
          <cis e   gis>_\markup{"2°"}
          <dis fis ais>_\markup{"3°"}
          <e   gis b  >_\markup{"4°"}
          <fis ais cis>_\markup{"5°"}
          <gis b   dis>_\markup{"6°"}
          <ais cis e  >_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{C\texorpdfstring{\sharp{}}{\#}/D\texorpdfstring{\flat{}}{b} Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <cis eis gis>1_\markup{"1°"}
          <dis fis ais>_\markup{"2°"}
          <eis gis bis>_\markup{"3°"}
          <fis ais cis>_\markup{"4°"}
          <gis bis dis>_\markup{"5°"}
          <ais cis eis>_\markup{"6°"}
          <bis dis fis>_\markup{"7°"}
          \bar "||"
          <des f   aes>1_\markup{"1°"}
          <ees ges bes>_\markup{"2°"}
          <f   aes c  >_\markup{"3°"}
          <ges bes des>_\markup{"4°"}
          <aes c   ees>_\markup{"5°"}
          <bes des f  >_\markup{"6°"}
          <c   ees ges>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{D\texorpdfstring{\sharp{}}{\#}/E\texorpdfstring{\flat{}}{b} Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <dis   fisis ais>1_\markup{"1°"}
          <eis   gis   bis>_\markup{"2°"}
          <fisis ais   cisis>_\markup{"3°"}
          <gis   bis   dis>_\markup{"4°"}
          <ais   cisis eis>_\markup{"5°"}
          <bis   dis   fisis>_\markup{"6°"}
          <cisis eis   gis>_\markup{"7°"}
          \bar "||"
          <ees g   bes>1_\markup{"1°"}
          <f   aes c  >_\markup{"2°"}
          <g   bes d  >_\markup{"3°"}
          <aes c   ees>_\markup{"4°"}
          <bes d   f  >_\markup{"5°"}
          <c   ees g  >_\markup{"6°"}
          <d   f   aes>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{F\texorpdfstring{\sharp{}}{\#}/G\texorpdfstring{\flat{}}{b} Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <fis ais cis>1_\markup{"1°"}
          <gis b   dis>_\markup{"2°"}
          <ais cis eis>_\markup{"3°"}
          <b   dis fis>_\markup{"4°"}
          <cis eis gis>_\markup{"5°"}
          <dis fis ais>_\markup{"6°"}
          <eis gis b  >_\markup{"7°"}
          \bar "||"
          <ges bes des>1_\markup{"1°"}
          <aes ces ees>_\markup{"2°"}
          <bes des f  >_\markup{"3°"}
          <ces ees ges>_\markup{"4°"}
          <des f   aes>_\markup{"5°"}
          <ees ges bes>_\markup{"6°"}
          <f   aes ces>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{G\texorpdfstring{\sharp{}}{\#}/A\texorpdfstring{\flat{}}{b} Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <gis bis dis>1_\markup{"1°"}
          <ais cis eis>_\markup{"2°"}
          <bis dis fisis>_\markup{"3°"}
          <cis eis gis>_\markup{"4°"}
          <dis fisis ais>_\markup{"5°"}
          <eis gis bis>_\markup{"6°"}
          <fisis ais cis>_\markup{"7°"}
          \bar "||"
          <aes c   ees>1_\markup{"1°"}
          <bes des f  >_\markup{"2°"}
          <c   ees g  >_\markup{"3°"}
          <des f   aes>_\markup{"4°"}
          <ees g   bes>_\markup{"5°"}
          <f   aes c  >_\markup{"6°"}
          <g   bes des>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}

\subsection{A\texorpdfstring{\sharp{}}{\#}/B\texorpdfstring{\flat{}}{b} Major Triads}

\begin{center}
  \begin{lilypond}
    \score {
      <<
      \new Staff {
        \relative c' {
          <ais cisis eis>1_\markup{"1°"}
          <bis dis fisis>_\markup{"2°"}
          <cisis eis gisis>_\markup{"3°"}
          <dis fisis ais>_\markup{"4°"}
          <eis gisis bis>_\markup{"5°"}
          <fisis ais cisis>_\markup{"6°"}
          <gisis bis dis>_\markup{"7°"}
          \bar "||"
          <bes d   f  >1_\markup{"1°"}
          <c   ees g  >_\markup{"2°"}
          <d   f   a  >_\markup{"3°"}
          <ees g   bes>_\markup{"4°"}
          <f   a   c  >_\markup{"5°"}
          <g   bes d  >_\markup{"6°"}
          <a   c   ees>_\markup{"7°"}
          \bar "||"
        }
      }      
      >>
    }
  \end{lilypond}
\end{center}


\subsection{Guitar Natural Major Chords}
\begin{lilypond}
  \include "predefined-guitar-fretboards.ly"
  
  naturalchords = \chordmode {
    c' d e f g a b \break
  }
  
  \score {
    <<
    \new ChordNames {
      \naturalchords
    }
    \new FretBoards {
      \naturalchords
    }
    >>
  }
\end{lilypond}

\section{Natural Minor Chords}
\index{chords!natural minor}

\begin{table}
  \caption{Tones and semitones of the natural minor chords}
  \label{tab:natur-minor-chord}
  \centering

  \begin{tabular}{*{8}{|c}|}
    \hline
    A &       & C &    & E &       & A \\
    \hline
    \ & 1\textonehalf{}T &   & 2T &   & 2\textonehalf{}T &   \\
    \hline
    \ & m3    &   & M3 &   & P4    &   \\
    \hline
  \end{tabular}
\end{table}

%% 1 1/2T  -  2T 

\begin{lilypond}
  \score {
    <<
    \chords {
      c1:m d:m e:m f:m g:m a:m b:m
    }
    \new Staff {
      \relative c' {        
        <a c e>1
        <b d fis>
        <c ees g>
        <d f a>
        <e g b>
        <f aes c>
        <g bes d>
      }
    }
    >>
  }
\end{lilypond}

\subsection{Guitar Minor Chords}
\begin{lilypond}
  \include "predefined-guitar-fretboards.ly"
  
  naturalchords = \chordmode {
    a':m b:m c:m d:m e:m f:m \break
  }
  
  \score {
    <<
    \new ChordNames {
      \naturalchords
    }
    \new FretBoards {
      \naturalchords
    }
    >>
  }
\end{lilypond}

\subsection{Guitar 7th Minor Chords}
\begin{lilypond}
  \include "predefined-guitar-fretboards.ly"
  
  naturalchords = \chordmode {
    c':7 d:7 e:7 f:7 g:7 a:7 b:7 \break
  }
  
  \score {
    <<
    \new ChordNames {
      \naturalchords
    }
    \new FretBoards {
      \naturalchords
    }
    >>
  }
\end{lilypond}
