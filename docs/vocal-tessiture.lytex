\section{Voice Classification}
\emph{It is important to not to hurry to classify the voice.} This section is provided as a reference and not to provide any means to determine which kind of voice we possess. Therefore, avoid any premature diagnoses about the tessiture of any voice to prevent any harm to the student. Just begin with any comfortable (middle) part of the voice and work upwards and downwards~\cite{mckinney82}.

\warning{\emph{It is important to not to hurry to classify the voice!}
  
  Work your voice from a comfortable range, and work it upwards and downards.}

The Score~\ref{score:vocal-types-range} shows three intervals, which establish the \textbf{range} of the different type of voices. The fist interval is the twelfth range which that particular singer can handle, the two octaves that a professional singer should have, and the extreme ranges sometimes demanded. It is important to note, that the range may vary between individuals, and therefore may not be precise. \index{range}\index{voice range}

\begin{score}
  \centering
  \begin{lilypond}
    \score{
      <<
      \new Staff
      \relative c' {
        \clef "treble"
        e1  | f   | g   \bar "||"
        fis | aes | bes \bar "||"
        a   | c   | ees \bar "||"
        e   | g   | bes \bar "||"
        <c,, g''> | a'' | c \bar "||"
        <d,, a''> | <c c''> | f'' \bar "||"
      }
      \new Staff
      \relative c {
        \clef "bass"
        a1_\markup {"Bass"} | f   | c   \bar "||"
        b'_\markup{"Baritone"} | aes | g   \bar "||"
        d'_\markup{"Tenor"} | c   | bes \bar "||"
        a'_\markup{"Contralto"} | g   | e   \bar "||"
        s_\markup{"Mezzo"}  | a   | g   \bar "||"
        s_\markup{"Soprano"}  | s   | a   \bar "|."
      }
      >>
    }  
  \end{lilypond}
  \caption{Vocal types and their ranges.}
  \label{score:vocal-types-range}
\end{score}

From the mentioned notes, the Table~\ref{tab:voice-ranges} can be deduced. This table mention the different voice types and their ranges in the same way.

\begin{table}
  \caption{Voice types and their ranges.}
  \label{tab:voice-ranges}
  \centering
  \begin{tabular}{|c|c|c|c|}
    \hline
    Voice type & ``practical'' & ``ideal'' & ``extreme'' \\
    \hline
    Bass      & A2 to E4 & F2 to F4 & C2 to G4 \\
    Baritone  & B2 to F\sharp4 & A\flat2 to A\flat4 & G2 to B\flat4 \\
    Tenor     & D3 to A4 & C3 to C5 & B\flat2 to E\flat5 \\
    Contralto & A3 to E5 & G3 to G5 & E3 to B\flat5 \\
    Mezzo     & C4 to G5 & A3 to A5 & G3 to C6 \\
    Soprano   & D4 to A5 & C4 to C6 & A3 to F6 \\
    \hline
  \end{tabular}
\end{table}

Range is not the same as tessitura. \textbf{Tessitura} is not the total range, it is the part of the range comfortable or most used by the singer. Some singer may sing a song comfortably, others with the same range may feel the tune more demanding if some of the notes are very high. \index{tessitura}\index{voice tessitura}
