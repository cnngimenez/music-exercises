# Copyright 2019 Christian Gimenez

# Author: Christian Gimenez   

# Makefile

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SCORENAMES := $(basename $(notdir $(wildcard scores/*.ly)))
PDF_FILENAMES := $(addsuffix .pdf, $(basename $(SCORENAMES)))
PDF_SCORES := $(addprefix produced/pdf/, $(PDF_FILENAMES))

MIDI_FILENAMES := $(addsuffix .midi, $(basename $(SCORENAMES)))
MIDI_SCORES := $(addprefix produced/midi/, $(MIDI_FILENAMES))

produced/pdf/%.pdf: scores/%.ly
	echo "Compiling $< into $@"
	lilypond -o $(basename $@) $<
	mv -v produced/pdf/*.midi produced/midi/

produced/midi/%.midi: scores/%.ly
	echo "Compiling $< into $@"
	lilypond -o $(basename $@) $<
	mv -v produced/midi/*.pdf produced/pdf/

produced/ogg/%.ogg: produced/midi/%.midi
	make -f Makefile-ogg.make $@	

.PHONY: clean ogg midi pdf all produced main-doc
.SILENT: test

all: pdf midi ogg main-doc

$(PDF_SCORES): | produced
$(MIDI_SCORES): | produced

midi: $(MIDI_SCORES)

ogg: midi | produced
	make -f Makefile-ogg.make

pdf: $(PDF_SCORES)

produced:
	mkdir -p produced/pdf
	mkdir -p produced/midi
	mkdir -p produced/ogg

clean:
	rm -r -f -v produced

main-doc: produced/music-exercise.pdf

produced/music-exercise.pdf:
	make -C './docs/' all
	cp -v ./docs/texs/main.pdf produced/music-exercise.pdf

test:
	echo $(SCORENAMES)
	echo "- PDF compilation variables:"
	echo $(PDF_FILENAMES)
	echo $(PDF_SCORES)
	echo "- MIDI compilation variables:"
	echo $(MIDI_FILENAMES)
	echo $(MIDI_SCORES)
	echo "- Ogg compilation variables:"
	echo $(OGG_FILENAMES)
	echo $(OGG_SCORES)

