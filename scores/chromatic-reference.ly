%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

\version "2.22.2"

\include "../includes/date-and-times.ly"
\include "../includes/format.ly"

\header {
  title = "Chromatic Scale with Frequency Reference"
  composer = "Christian Gimenez"
  %% subtitle=\date
  copyright= \markup {
    \center-column {
      \justified-lines{
        Chromatic Scale with Frequency Reference by Christian Gimenez is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
      }
      "License information: http://creativecommons.org/licenses/by-sa/4.0/"
      "Visit https://gitlab.com/cnngimenez/music-exercises for more information and exercises."
    }} 
}

\markuplist {
  \date 
  \justified-lines {
    These are chromatic scales and their notes with scale and frequencies below. Different clefs are used to show their notes. First, the trebble (G or \italic{Sol}) clef is shown. Then, the next most common staves used for piano, treble and bass (G and F) clefs.
  }
}

\markup \mytitle "Treble clef"
\score {
  \include "../contents/chromatic-reference-treble.ly"
}
\score {
  \include "../contents/chromatic-reference-treble.ly"
  \midi{}
}

\markup \mytitle "Treble (G) and bass (F) clefs"
\score {
  \include "../contents/chromatic-reference-treble_and_bass.ly"
}
\score {
  \include "../contents/chromatic-reference-treble_and_bass.ly"
  \midi{}
}

\markuplist {
  \mytitle "Treble suboctave clef"
  \justified-lines {
    This clef is commonly used for some instrumments such as guitars and for the tenor voices. It is a treble clef, but the notes are one octave lower.
  }
}
\score {
  \include "../contents/chromatic-reference-treble_suboctave.ly"
}
\score {
  \include "../contents/chromatic-reference-treble_suboctave.ly"
  \midi{}
}
