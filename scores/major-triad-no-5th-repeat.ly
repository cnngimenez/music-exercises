%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

\version "2.22.2"

\include "../includes/date-and-times.ly"
\include "../includes/format.ly"

\header {
  title = "Major Triad Exercise"
  composer = "Christian Gimenez"
  %% subtitle=\date
  copyright= \markup {
    \center-column {
      \justified-lines{
        Major Triad by Christian Gimenez is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
      }
      "License information: http://creativecommons.org/licenses/by-sa/4.0/"
      "Visit https://gitlab.com/cnngimenez/music-exercises for more information and exercises."
    }} 
}

\markuplist {
  \date 
  \justified-lines {
    Without glissando.
  }
  \justified-lines {
    Warning. When singing: \bold {do not force your voice!} If the notes are too low or too high for you, do not sing them until asking your music teacher/coach. Stable voice (frequency/pitch) whenever possible.
  }
  \justified-lines {
    Volume must be comfortable and stable: it must not be too loud nor too quiet. Try to avoid shouting or whispering. Try not to make the volume louder when playing higher notes.
  }
  \justified-lines {
    We recommend to read the ''Music Exercise'' complete documentation, or following your teacher advice, before singing this exercise.
  }
}

\score { 
  \include "../contents/major-triad-no-5th-repeat.ly"
}
\score { 
  \include "../contents/major-triad-no-5th-repeat.ly"
  \midi{}
}

\pageBreak

\markup{ \mytitle "Major Triad for Bass" }
\markup{Bass singer range usually is \bold{from F2 to F4}. Avoid forcing your voice at the extreme notes!}
\score {
  \include "../contents/major-triad-bass-no-5th-repeat.ly"
}
\score {
  \include "../contents/major-triad-bass-no-5th-repeat.ly"
  \midi{}
}

\markup{ \mytitle "Major Triad for Baritone" }
\markup{Baritone singer range usually is \bold{from A\flat 2 to A\flat 4}. Avoid forcing your voice at the extreme notes!}
\score {
  \include "../contents/major-triad-baritone-no-5th-repeat.ly"
}
\score {
  \include "../contents/major-triad-baritone-no-5th-repeat.ly"
  \midi{}
}
\pageBreak

\markup{ \mytitle "Major Triad for Tenor" }
\markup{Tenor singer range usually is \bold{from C3 to C5}. Avoid forcing your voice at the extreme notes!}
\score {
  \include "../contents/major-triad-tenor-no-5th-repeat.ly"
}
\score {
  \include "../contents/major-triad-tenor-no-5th-repeat.ly"
  \midi{}
}

\markup{ \mytitle "Major Triad for Contralto" }
\markup{Contralto singer range usually is \bold{from G3 to G5}. Avoid forcing your voice at the extreme notes!}
\score {
  \include "../contents/major-triad-contralto-no-5th-repeat.ly"
}
\score {
  \include "../contents/major-triad-contralto-no-5th-repeat.ly"
  \midi{}
}
\pageBreak

\markup{ \mytitle "Major Triad for Mezzo" }
\markup{Mezzo singer range usually is \bold{from A3 to A5}. Avoid forcing your voice at the extreme notes!}
\score {
  \include "../contents/major-triad-mezzo-no-5th-repeat.ly"
}
\score {
  \include "../contents/major-triad-mezzo-no-5th-repeat.ly"
  \midi{}
}

\markup{ \mytitle "Major Triad for Soprano" }
\markup{Soprano singer range usually is \bold{from C4 to C6}. Avoid forcing your voice at the extreme notes!}
\score {
  \include "../contents/major-triad-soprano-no-5th-repeat.ly"
}
\score {
  \include "../contents/major-triad-soprano-no-5th-repeat.ly"
  \midi{}
}