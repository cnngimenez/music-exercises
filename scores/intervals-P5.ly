%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

\version "2.22.2"

date=#(string-append "Fecha: " (strftime "%d/%m/%Y" (localtime (current-time))))
\header {
  title = "Interval Exercise: P5"
  composer = "Christian Gimenez"
  %% subtitle=\date
  copyright= \markup {
    \center-column {
      \justified-lines{
        Intervals: P5 by Christian Gimenez is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
      }
      "License information: http://creativecommons.org/licenses/by-sa/4.0/"
      "Visit https://gitlab.com/cnngimenez/music-exercises for more information and exercises."
    }} 
}

\markup { \date }
\markup {Without glissando.}

\score {
  \include "../contents/intervals-P5-score1.ly"
}
\score {
  \include "../contents/intervals-P5-score2.ly"
  \midi{}
}


%% ----------
%% Glissando

\markup{ With glissando.}

\score {
  \include "../contents/intervals-P5-score2.ly"
}

%% No need to create MIDI: timidity cannot do glissando or lagated notes :/