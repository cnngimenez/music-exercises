%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

\version "2.22.2"

\include "../includes/date-and-times.ly"
\include "../includes/format.ly"

\header {
  title = "Major Scales"
  composer = "Christian Gimenez"
  %% subtitle=\date
  copyright= \markup {
    \center-column {
      \justified-lines{
        Major Scales by Christian Gimenez is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
      }
      "License information: http://creativecommons.org/licenses/by-sa/4.0/"
      "Visit https://gitlab.com/cnngimenez/music-exercises for more information and exercises."
    }} 
}

\markup \mytitle "Major scales of natural notes"
\score {
  \include "../contents/major-scales-naturals.ly"
}
\score {
  \include "../contents/major-scales-naturals.ly"
  \midi{}
}

\markup \mytitle "Major scales of natural and altered notes"
\score {
  \include "../contents/major-scales-alterations.ly"
}
\score {
  \include "../contents/major-scales-alterations.ly"
  \midi{}
}

\markup \mytitle "Major scales of natural notes with fourth notes"
\score {
  \include "../contents/major-scales-naturals-fourths.ly"
}
\score {
  \include "../contents/major-scales-naturals-fourths.ly"
  \midi{}
}

\markup \mytitle "Major scales of natural and altered notes with fourth notes"
\score {
  \include "../contents/major-scales-alterations-fourths.ly"
}
\score {
  \include "../contents/major-scales-alterations-fourths.ly"
  \midi{}
}
