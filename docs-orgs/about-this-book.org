# This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
# License information: http://creativecommons.org/licenses/by-sa/4.0/

# It is recommended to maintain cache enabled to avoid repeated runs of ob-lilypond of unmodified source blocks.
# Org-mode provides a security measure to ask before executing source blocks. See `org-confirm-babel-evaluate'
# variable documentation if you want to configure this feature.

#+PROPERTY: header-args:lilypond :prologue (org-babel-ref-resolve "settings[]") :noweb yes :exports results :cache yes

* About This Book
#+latex: \label{chap:about-this-book}
This chapter describes writing and format conventions used on this book. This book kind of contents and languages are also discussed, as well as their written representation.

This book homepage can be visited by the URL: {{{homepage}}}

# The source code is available under the license indicated in the first pages of this book. More about the book develoment and its source code can be found at Chapter [[chap:how-book-devel]].

** Font, text size, and line spacing
The font, size, and line spacing were specifically selected for easy and better readability. Sans serif font (in particular, Latin Modern Sans), 14 pt size, and 1.25 line spacing were used to help people with reading difficulties, such as dyslexia. See https://webaim.org/techniques/fonts/ for more information.

There is no perfect font, or size, nor a perfect decision regarding the type of format and text. However, this book source code is free and open source (see [[*License]]), the authors allow to modify and to create new editions to fit other needs, as long as the license and authors are respected. Therefore, it is possible to create new versions of this book with other styles and formatting, to suit the needs for other audiences.

** Format and Writing Conventions
#+latex: \label{sec:forma-conve}
This sections describes the format and structure conventions used in this book. This conventions may not be strict, but are the prefered way used to write this work. 

Titles are written with initial uppercases. 

Relevant concepts are in *bold* to help the reader to find them (/emphasis/ are not encouraged). One or several index entries are used when this important concepts are mentioned. Then, the reader can search a concept or term on the Index @@latex:(see Index at page \pageref{chap:index})@@, go to the indicated page and find it easily.

Musics scores are written inline and centered when they are short and do not disturb the flow of - the reading. More than two lines of staves, are used in a "floating box" environments which position itself on the top or bottom page whenever it is near and there is space, or in another page. For example, a large code such as [[fig:score-atb-2]] is a floating box, but a simpler stave with a scale like this one:

#+name: score-atb-1
#+begin_src lilypond :file lilypond/atb-1.png
  \score {
    \new Staff \relative c' {
      \clef "treble"    
      c8 d   e   f   g   a   b   c  |
    }
  }
#+end_src

#+attr_latex: :height 1cm
#+RESULTS[3a3821a38463b6ea490d9c08ed25c09f637896c0]: score-atb-1
[[file:lilypond/atb-1.png]]

This inline stave can be found inline, expecting not to disturb the reader too much. The eye movement should be continuous, and the understanding of the paragraphs were not to be disturbed.

#+name: score-atb-2
#+begin_src lilypond :file lilypond/atb-2.png
  \score {
    \new Staff \relative c' {
      \clef "treble"    
      c8 d   e   f   g   a   b   c  |
      d,   e   fis g   a   b   cis d |
      e,   fis gis a   b   cis dis e |
      f,   g   a   bes c   d   e   f |
      g,   a   b   c   d   e   fis g \bar "|."
    }
  }
#+end_src

#+name: fig:score-atb-2
#+caption: An example score.
#+RESULTS[d84f848ea9b7bb31d03351ad5e58f4c4e29cb984]: score-atb-2
[[file:lilypond/atb-2.png]]

Moreover, to refer to a particular score in this book, such as the previous one, the "Figure NUMBER" format are used (for PDF or \LaTeX{} generated resources), in a similar way as the figures, chapters or any object of this book.

** Content Conventions
All content are synthesis from other formal resources. Academic and technical resources are preferred, such as journal or congress papers, books, and any material that were under some peer-review process. The paragraphs and texts that refers to these contents are cited and references. See the Reference section of this book 

This book may have reference content, as well as curiosities or anything about music theory and singing. Other instruments may be included as well in their respective chapters.

** Multiple Languages :noexport:
The initial language of this book is english, but a translation of a portion of this book is also considered. At some point of the development of this work, a new entire \TeX{} book may be created to provide a better framework for a particular language. Thus, that other project can provide the format and writing customs of that language without clashing with the current one. For example, spanish speakers may use different criteria for particular language usages, such as doble quotes: english double quotes are "example", but spanish uses <<example>> tipically (however some book editors may use others).

Any part of this book can have a section or a chapter in another language, provided that language book is not present. The language name and a represenative emoji flag is placed before the text. \cref{sec:differ-langu} explains this for development the book. The following example shows how a text in spanish (particularly, argentinian spanish) is described in this book:

\begin{flushright}
  Español \emoji{flag-spain}\emoji{flag-argentina}.
\end{flushright}

Este es un ejemplo de una porción de texto escrito en español. Particularmente, se utiliza el español de argentina.




#+name: settings
#+begin_src lilypond :exports none
  \version "2.24.1"

  #(ly:set-option 'use-paper-size-for-page #f)
  #(ly:set-option 'tall-page-formats 'png)
  \header {    
    tagline = ##f
  }
#+end_src

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty 
#+STARTUP: indent fninline latexpreview
#+OPTIONS: H:3 num:t title:nil toc:nil \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick
#+OPTIONS: makeindex:t

# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
