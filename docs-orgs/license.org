# This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
# License information: http://creativecommons.org/licenses/by-sa/4.0/
* License
This chapter describes the license of this work and all the resources used.

** This Work License
This work "{{{title}}}" by {{{author}}} is licensed under a Creative Commons - Attribution - ShareAlike 4.0 International License (CC-by-SA 4.0 International).

** Images Licenses
#+attr_latex: :wrap t :width 100px
[[../docs/imgs/Cover-100px.jpg]]

The cover image is called: "Saxophonist, District 54 Jazz Band Recording Session 11, Solid Sound Recording Studio, Hoffman Estates, IL, 2011." by H. Michael Miley.
This image is licensed under the Creative Commons Attribution-Share Alike 2.0 Generic license.
Obtained from [[https://commons.wikimedia.org/wiki/File:Saxophonist_-_District_54_Jazz_Band_Recording_Session_11_-_Solid_Sound_Recording_Studio,_Hoffman_Estates,_IL,_2011.jpg][this Wikimedia page]].

#+name: settings
#+begin_src lilypond :exports none
  \version "2.24.1"

  #(ly:set-option 'use-paper-size-for-page #f)
  #(ly:set-option 'tall-page-formats 'png)
  \header {    
    tagline = ##f
  }
#+end_src

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty 
#+STARTUP: indent fninline latexpreview
#+OPTIONS: H:3 num:t title:nil toc:nil \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick
#+OPTIONS: makeindex:t

# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
