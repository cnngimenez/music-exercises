%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 70
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r2 c4 <c e g>4 |
  c8 d  e f  g f  e d  | c4 
  
  r4 d4 <d fis a>4 |
  d8 e  fis g  a g  fis e  | d4
  
  r4^\markup{\mytime 4 70 4} e4 <e gis b>4 |
  e8 fis  gis a  b a gis fis | e4 

  r4 f4 <f a c>4 | 
  f8 g  a bes  c bes  a g  | f4 

  r4^\markup{\mytime 8 70 4} g4 <g b d >4 |
  g8 a  b c  d c  b a  | g4

  r4 a4 <a cis e>4 |
  a8 b  cis d  e d  cis b | a4

  r4 b4 <b dis fis>4 |
  b8 cis  dis e  fis e  dis cis | b4

  r4 c4 <c e g>4 |
  c8 d  e f  g f  e d  c4 r2. \bar "||"

  \clef "treble"

  r2^\markup{\mytime 16 70 4} c4 <c e g>4 |
  c8 d  e f  g f  e d  | c4 

  r4 d4 <d fis a>4 |
  d8 e  fis g  a g  fis e | d4

  r4^\markup{\mytime 20 70 4} e4 <e gis b>4 |
  e8 fis gis a b a gis fis | e4

  r4 f4 <f a c>4 |
  f8 g a bes c bes a g f4 r2.\bar "|."

}
\new Lyrics \lyricmode {
  "C3"1 "-"1 "-"2 "D3"2 "-"1 "-"2 "E3"2 "-"1 "-"2 "F3"2 "-"1 "-"2
  "G3"2 "-"1 "-"2 "A3"2 "-"1 "-"2 "B3"2 "-"1 "-"2 "C4"2 "-"1 "-"1
  
  "-"2 "C4"2 "-"1 "-"2 "D4"2 "-"1 "-"2 "E4"2 "-"1 "-"2 "F4"1
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/up-to-5th.ly"
% End:
