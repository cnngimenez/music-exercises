%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 70
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r2 c4 <c e g>4 |
  g'8 f  e d  c4 r4
  
  r2 d4 <d fis a>4 |
  a'8 g  fis e  d4 r4 |
  
  r2 e4 <e gis b>4 |
  b'8 a gis fis e4 r4 |

  r2 f4 <f a c>4 | 
  c'8 bes  a g  f4 r4 |

  r2 g4 <g b d >4 |
  d'8 c  b a g4 r4 |

  r2 a4 <a cis e>4 |
  e'8 d  cis b a4 r4 |

  r2 b4 <b dis fis>4 |
  fis'8 e  dis cis b4 r4 |

  r2 c4 <c e g>4 |
  g'8 f  e d  c4 r4 \bar "||"

  \clef "treble"

  r2 c4 <c e g>4 |
  g'8 f  e d  c4 r4 |

  r2 d4 <d fis a>4 |
  a'8 g  fis e d4 r4 |

  r2 e4 <e gis b>4 |
  b'8 a gis fis e4 r4 |

  r2 f4 <f a c>4 |
  c'8 bes a g f4 r4\bar "|."

}
\new Lyrics \lyricmode {
  "C3"1 "-"1 "D3"1 "-"1 "E3"1 "-"1 "F3"1 "-"1 
  "G3"1 "-"1 "A3"1 "-"1 "B3"1 "-"1 "C4"1 "-"1   
  "C4"1 "-"1 "D4"1 "-"1 "E4"1 "-"1 "F4"1 "-"1
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/down-5th.ly"
% End:
