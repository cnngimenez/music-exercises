%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 50
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r1 |
  c8 g'  c, e   c d   c r |
  d8 a'  d, fis d e   d r |
  e8 b'  e, gis e fis e r |
  f8 c'  f, a   f g   f r |
  g  d'  g, b   g a   g r |
  a  e'  a, cis a b   a r |
  b  fis' b, dis b cis b r |
  
  \clef "treble" 
  c8 g' c, e   c d    c r |
  d8 a'  d, fis d e   d r |
  e8 b'  e, gis e fis e r |
  f8 c'  f, a   f g   f r |
 \bar "||"

}
\new Lyrics \lyricmode {
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/unga.ly"
% End:
