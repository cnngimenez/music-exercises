%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

<<
  \new Staff \relative c' {
    \clef treble
    s1 s1 s1 s1
    s1 s1
    c4 cis d dis e f fis g gis a ais b |
    c4 cis d dis e f fis g gis a ais b |
    c4 cis \bar "|."    
  }
  \addlyrics {  
    "C4" "C#4" "D4" "D#4" "E4" "F4" "F#4" "G4" "G#4" "A4" "A#4" "B4"
    "C5" "C#5" "D5" "D#5" "E5" "F5" "F#5" "G5" "G#5" "A5" "A#5" "B5"
    "C6" "C#6"
  }
  \addlyrics {
    %% C4 to B4
    "261.63" "277.18" "293.66" "311.13" "329.63" "349.23" "369.99" "392.00" "415.30" "440.00" "466.16" "493.88"
    %% C5 to B5
    "523.25" "554.37" "587.33" "622.25" "659.26" "698.46" "739.99" "783.99" "830.61" "880.00" "932.33" "987.77"
    %% C6 C#6
    "1046.50" "1108.73"
  }
  \new Staff \relative c, {
    \clef bass
    c4 cis d dis e f fis g gis a ais b |
    c4 cis d dis e f fis g gis a ais b |
    s1 s1 s1 s1
    s1 s1 s4 s4 \bar "|."    
  }
  \addlyrics {
    "C2" "C#2" "D2" "D#2" "E2" "F2" "F#2" "G2" "G#2" "A2" "A#2" "B2"
    "C3" "C#3" "D3" "D#3" "E3" "F3" "F#3" "G3" "G#3" "A3" "A#3" "B3"
  }
  \addlyrics {
    %% C2 to B2
    "65.41" "69.30" "73.42" "77.78" "82.41" "87.31" "92.50" "98.00" "103.83" "110.00" "116.54" "123.47"
    %% C3 to B3
    "130.81" "138.59" "146.83" "155.56" "164.81" "174.61" "185.00" "196.00" "207.65" "220.00" "233.08" "246.94"
  }  
>>
  
% Local Variables:
% LilyPond-master-file: "../scores/chromatic-reference.ly"
% End:
