%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 4)
#(set-tempo 80)

<<
\new Staff
\relative c, {
  \key c \major
  \time 4/4
  \tempo 4 = 80
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  c8(e)   g(e)   c4 r4 | d8(fis) a(fis) d4 r4 |
  e8(gis) b(gis) e4 r4 | f8(a)   c(a)   f4 r4 |
  g8(b)   d(b)   g4 r4 | a8(cis) e(cis) a4 r4 |
  b8(dis) fis(dis) b4 r4 | c8(e) g(e) c4 r4 \bar "||"
  
  c8^\markup{\mytime 8 80 4} (e)   g(e)   c4 r4 | d8(fis) a(fis) d4 r4 |
  e8(gis) b(gis) e4 r4 | f8(a)   c(a)   f4 r4 |
  g8(b)   d(b)   g4 r4 | a8(cis) e(cis) a4 r4 |
  b8(dis) fis(dis) b4 r4 | c8(e) g(e) c4 r4 \bar "||"

  \clef "treble"

  c8^\markup{\mytime 16 80 4} (e)   g(e)   c4 r4 | d8(fis) a(fis) d4 r4 |
  e8(gis) b(gis) e4 r4 | f8(a)   c(a)   f4 r4 |
  g8(b)   d(b)   g4 r4 | a8(cis) e(cis) a4 r4 |
  b8(dis) fis(dis) b4 r4 | c8(e) g(e) c4 r4 \bar "||"

  c8^\markup{\mytime 24 80 4} (e)   g(e)   c4 r4 | d8(fis) a(fis) d4 r4 |
  e8(gis) b(gis) e4 r4 | f8(a)   c(a)   f4 r4 |
  g8(b)   d(b)   g4 r4 | a8(cis) e(cis) a4 r4 |
  b8(dis) fis(dis) b4 r4 | c8(e) g(e) c4 r4 \bar "|."


}
\new Lyrics \lyricmode {
  "C2"1 ""1 ""1 ""1 ""1 ""1 ""1 ""1
  "C3"1 ""1 ""1 ""1 ""1 ""1 ""1 ""1
  "C4"1 ""1 ""1 ""1 ""1 ""1 ""1 ""1
  "C5"1 ""1 ""1 ""1 ""1 ""1 ""1 ""1
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/major-triad-no-5th-repeat.ly"
% End:
