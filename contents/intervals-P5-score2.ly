%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

\new Staff
\relative c, {
  \key c \major
  \time 4/4
  \tempo 4 = 80
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1) 
  
  \clef "bass"
  
  c8\glissando g'\glissando c, r r2 | d8\glissando a'\glissando d, r r2 |
  e8\glissando b'\glissando e, r r2 | f8\glissando c'\glissando f, r r2 |
  g8\glissando d'\glissando g, r r2 | a8\glissando e'\glissando a, r r2 |
  b8\glissando fis'\glissando b, r r2 | c8\glissando g'\glissando c, r r2 |
  \bar "||"
  
  c8\glissando g'\glissando c, r r2 | d8\glissando a'\glissando d, r r2 |
  e8\glissando b'\glissando e, r r2 | f8\glissando c'\glissando f, r r2 |
  g8\glissando d'\glissando g, r r2 | a8\glissando e'\glissando a, r r2 |
  b8\glissando fis'\glissando b, r r2 | c8\glissando g'\glissando c, r r2 |
  \bar "||"

  \clef "treble"
  
  c8\glissando g'\glissando c, r r2 | d8\glissando a'\glissando d, r r2 |
  e8\glissando b'\glissando e, r r2 | f8\glissando c'\glissando f, r r2 |
  g8\glissando d'\glissando g, r r2 | a8\glissando e'\glissando a, r r2 |
  b8\glissando fis'\glissando b, r r2 | c8\glissando g'\glissando c, r r2 |
  \bar "||"

  c8\glissando g'\glissando c, r r2 | d8\glissando a'\glissando d, r r2 |
  e8\glissando b'\glissando e, r r2 | f8\glissando c'\glissando f, r r2 |
  g8\glissando d'\glissando g, r r2 | a8\glissando e'\glissando a, r r2 |
  b8\glissando fis'\glissando b, r r2 | c8\glissando g'\glissando c, r r2 |
  \bar "|."
}

% Local Variables:
% LilyPond-master-file: "../scores/mayor-triad.ly"
% End:
