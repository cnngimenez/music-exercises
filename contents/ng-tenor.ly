%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 50
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r1 |
  c8 e     c g'   c, e   c r |
  d8 fis   d a'   d, fis  d r |
  e8 gis   e b'   e, gis  e r |
  f8 a     f c'   f, a    f r |
  g  b     g d'   g, b    g r |
  a  cis   a e'   a, cis  a r |
  b  d     b fis' b, d    b r |
  
  \clef "treble" 
  c8 e     c g'   c, e    c r |
  d8 fis   d a'   d, fis  d r |
  e8 gis   e b'   e, gis  e r |
  f8 a     f c'   f, a    f r |
 \bar "||"

}
\new Lyrics \lyricmode {
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/ng.ly"
% End:
