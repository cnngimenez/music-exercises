%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 4)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 70
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r2 c4 <c e g>4 |
  c8(e) g(e) c2 |
  
  r2 d4 <d fis a>4 |
  d8(fis) a(fis) d2 |
  
  r2^\markup{\mytime 4 70 4} e4 <e gis b>4 |
  e8(gis) b(gis) e2 |

  r2 f4 <f a c>4 | 
  f8(a) c(a) f2 |

  r2^\markup{\mytime 8 70 4} g4 <g b d >4 |
  g8(b) d(b) g2 |

  r2 a4 <a cis e>4 |
  a8(cis) e(cis) a2 |

  r2 b4 <b dis fis>4 |
  b8(dis) fis(dis) b2 |

  r2 c4 <c e g>4 |
  c8(e) g(e) c2 \bar "||"

  \clef "treble"

  r2^\markup{\mytime 16 70 4} c4 <c e g>4 |
  c8(e) g(e) c2 |

  r2 d4 <d fis a>4 |
  d8(fis) a(fis) d2 |

  r2^\markup{\mytime 20 70 4} e4 <e gis b>4 |
  e8(gis) b(gis) e2 |

  r2 f4 <f a c>4 |
  f8(a) c(a) f2 \bar "|."

}
\new Lyrics \lyricmode {
  "C3"1 "-"1 "D3"1 "-"1 "E3"1 "-"1 "F3"1 "-"1
  "G3"1 "-"1 "A3"1 "-"1 "B3"1 "-"1 "C4"1 "-"1
  
  "C4"1 "-"1 "D4"1 "-"1 "E4"1 "-"1 "F4"1
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/major-triad-no-5th-repeat.ly"
% End:
