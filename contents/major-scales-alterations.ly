%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% <<
\new Staff
  \relative c, {
    \key c \major
    \time 4/4
    \tempo 4 = 80
    #(set! fourth-notes-per-time 4)
    #(set! tempo 80)
    \override Score.BarNumber.break-visibility = #end-of-line-invisible
    \context Score \applyContext #(set-bar-number-visibility 1)

    \clef "bass"
    
    c8   d   e   f   g   a   b   c   |
    d,   e   fis g   a   b   cis d   |
    e,   fis gis a   b   cis dis e   |
    f,   g   a   bes c   d   e   f   |
    g,   a   b   c   d   e   fis g   |
    a,   b   cis d   e   fis gis a   |
    b,   cis dis e   fis gis ais b   |
    c,8^\markup{\mytime 7 80 4}  d   e   f   g   a   b   c   \bar "||"
    
    c,8^\markup{\mytime 8 80 4}  d   e   f   g   a   b   c   |
    d,   e   fis g   a   b   cis d   |
    e,   fis gis a   b   cis dis e   |
    f,   g   a   bes c   d   e   f   |
    g,   a   b   c   d   e   fis g   |
    a,   b   cis d   e   fis gis a   |
    b,   cis dis e   fis gis ais b   |
    c,8^\markup{\mytime 14 80 4}   d   e   f   g   a   b   c   \bar "||"
    
    \clef "treble"
    
    c,8^\markup{\mytime 15 80 4}   d   e   f   g   a   b   c   |
    d,   e   fis g   a   b   cis d   |
    e,  fis gis a   b   cis dis e    |
    f,   g   a   bes c   d   e   f   |
    g,   a   b   cis dis e   fis g   |
    a,   b   cis d   e   fis gis a   |
    b,   cis dis e   fis gis ais b   |
    c,8^\markup{\mytime 21 80 4}   d   e   f   g   a   b   c   \bar "||"
  }
  \addlyrics {
    C D E F G A B C
    D E "F#" G A B "C#" D
    E "F#" "G#" A B "C#" "D#" E
    F G A Bb C D E F
    G A B C D E "F#" G
    A B "C#" D E "F#" "G#" A
    B "C#" "D#" E "F#" "G#" "A#" B
    C D E F G A B C

    C D E F G A B C
    D E "F#" G A B "C#" D
    E "F#" "G#" A B "C#" "D#" E
    F G A Bb C D E F
    G A B C D E "F#" G
    A B "C#" D E "F#" "G#" A
    B "C#" "D#" E "F#" "G#" "A#" B
    C D E F G A B C

    C D E F G A B C
    D E "F#" G A B "C#" D
    E "F#" "G#" A B "C#" "D#" E
    F G A Bb C D E F
    G A B C D E "F#" G
    A B "C#" D E "F#" "G#" A
    B "C#" "D#" E "F#" "G#" "A#" B
    C D E F G A B C
  }
%% >>  

% Local Variables:
% LilyPond-master-file: "../scores/major-scales.ly"
% End:
