%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

<<
\new Staff \relative c' {
  \clef treble
  
  c4 cis d dis e f fis g gis a ais b ||
  c4 cis d dis e f fis g gis a ais b ||
  c4 cis \bar "|."
}
\addlyrics {  
  "C4" "C#4" "D4" "D#4" "E4" "F4" "F#4" "G4" "G#4" "A4" "A#4" "B4"
  "C5" "C#5" "D5" "D#5" "E5" "F5" "F#5" "G5" "G#5" "A5" "A#5" "B5"
  "C6" "C#6"
}
\addlyrics {
  %% C4 to B4
  "261.63" "277.18" "293.66" "311.13" "329.63" "349.23" "369.99" "392.00" "415.30" "440.00" "466.16" "493.88"
  %% C5 to B5
  "523.25" "554.37" "587.33" "622.25" "659.26" "698.46" "739.99" "783.99" "830.61" "880.00" "932.33" "987.77"
  %% C6 C#6
  "1046.50" "1108.73"
}
>>

  
% Local Variables:
% LilyPond-master-file: "../scores/chromatic-reference.ly"
% End:
