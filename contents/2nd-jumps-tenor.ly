%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 50
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r1 |
  c8 d c e c f c g'(g) f e d c r4.
  d8 e d fis d g d a' (a) g fis e d r4.
  e8 fis e gis e a e b' (b) a gis fis e r4.
  f8 g f a f bes f c' (c) bes a g f r4.
  g8 a g b g c g d' (d) c b a g r4.
  a8 b a cis a d a e' (e) d cis b a r4.
  
  \clef "treble"
  b8 cis b dis b e b fis' (fis) e dis cis b r4.
  c8 d c e c f c g' (g) f e d c r4.
  c8 d c e c f c g' (g) f e d c r4.
  d8 e d fis d g d a' (a) g fis e d r4.
  e8 fis e gis e a e b' (b) a gis fis e r4.
  f8 g f a f bes f c' (c) bes a g f r4.
 \bar "||"

}
\new Lyrics \lyricmode {
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/2nd-jumps.ly"
% End:
