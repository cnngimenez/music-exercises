%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 4/4
  \tempo 4 = 50
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r1 |
  r2 c4 g' f8 e d c 
  r2 d4 a' g8 fis e d 
  r2 e4 b' a8 gis fis e 
  r2 f4 c' bes8 a g f 
  r2 g4 d' c8 b a g 
  r2 a4 e' d8 cis b a 
  r2 b4 fis' e8 dis cis b 
  
  \clef "treble" 
  r2 c4 g' f8 e d c 
  r2 d4 a' g8 fis e d 
  r2 e4 b' a8 gis fis e 
  r2 f4 c' bes8 a g f 
 \bar "||"

}
\new Lyrics \lyricmode {
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/oui.ly"
% End:
