%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

\new Staff
\relative c, {
  \key c \major
  \time 4/4
  \tempo 4 = 80
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"
  
  c8 g' c, r r2 | d8 a' d, r r2 | e8 b' e, r r2 | f8 c' f, r r2 |
  g8 d' g, r r2 | a8 e' a, r r2 | b8 fis' b, r r2 | c8 g' c, r r2 | \bar "||"

  c8 g' c, r r2 | d8 a' d, r r2 | e8 b' e, r r2 | f8 c' f, r r2 |
  g8 d' g, r r2 | a8 e' a, r r2 | b8 fis' b, r r2 | c8 g' c, r r2 | \bar "||"

  \clef "treble"
  
  c8 g' c, r r2 | d8 a' d, r r2 | e8 b' e, r r2 | f8 c' f, r r2 |
  g8 d' g, r r2 | a8 e' a, r r2 | b8 fis' b, r r2 | c8 g' c, r r2 | \bar "||"

  c8 g' c, r r2 | d8 a' d, r r2 | e8 b' e, r r2 | f8 c' f, r r2 |
  g8 d' g, r r2 | a8 e' a, r r2 | b8 fis' b, r r2 | c8 g' c, r r2 | \bar "|."

}

% Local Variables:
% LilyPond-master-file: "../scores/mayor-triad.ly"
% End:
