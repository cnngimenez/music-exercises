%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 8)
#(set-tempo 70)

<<
\new Staff
\relative c {
  \key c \major
  \time 12/8
  \tempo 4. = 50
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  c4. <c e g>4. g'8(e c)   c4. |
  
  d4. <d fis a>4.  a'8(fis d) d4. |
  
  e4. <e gis b>4. b'8(gis e) e4. |

  f4. <f a c>4. c'8(a f)   f4. |

  g4. <g b d >4. d'8(b g)   g4. |

  a4. <a cis e>4. e'8(cis a) a4. |

  b4. <b dis fis>4. fis'8(dis b) b4. |

  c4. <c e g>4.  g'8(e c) c4. \bar "||"

  \clef "treble"

  c4. <c e g>4.  g'8(e c)   c4. |

  d4. <d fis a>4. a'8(fis d) d4. |

  e4. <e gis b>4. b'8(gis e) e4. |

  f4. <f a c>4. c'8(a f)  f4. \bar "|."

}
\new Lyrics \lyricmode {
  "C3"1. "D3"1. "E3"1. "F3"1.
  "G3"1. "A3"1. "B3"1. "C4"1.
  
  "C4"1. "D4"1. "E4"1. "F4"1.
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/major-triad-downwards.ly"
% End:
