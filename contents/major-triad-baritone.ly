%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

%% \new FretBoards {
%%   \chordmode {
%%   g1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   c1 \override FretBoard.fret-diagram-details.orientation = #'landscape
%%   }
%% }

#(set! fourth-notes-per-time 6)
#(set! tempo 80)

<<
\new Staff
\relative c {
  \key c \major
  \time 12/8
  \tempo 4. = 70
  %% Compas numbers.
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \context Score \applyContext #(set-bar-number-visibility 1)

  \clef "bass"

  r2. a4. <a cis e>4. |
  a8(cis e) e(cis a) a4. r4. |

  r2. b4. <b dis fis>4. |
  b8(dis fis) fis(dis b) b4. r4. |

  r2. c4. <c e g>4. |
  c8(e g) g(e c) c4. r4. \bar "||"

  r2. c4. <c e g>4. |
  c8^\markup{\mytime 3 70 4} (e g)   g(e c)   c4. r4. |

  r2. d4. <d fis a>4. |
  d8(fis a) a(fis d) d4. r4. |

  r2. e4. <e gis b>4. |
  e8^\markup{\mytime 5 70 4}(gis b) b(gis e) e4. r4. |

  r2. f4. <f a c>4. |
  f8(a c)   c(a f)   f4. r4. |

  r2. g4. <g b d>4. |
  g8^\markup{\mytime 7 70 4}(b d)   d(b g)   g4. r4. |

  r2. a4. <a cis e>4. |
  a8(cis e) e(cis a) a4. r4. |

  r2. b4. <b dis fis>4. |
  b8(dis fis) fis(dis b) b4. r4. |

  r2. c4. <c e g>4. |
  c8(e g) g(e c) c4. r4. \bar "||"

  \clef "treble"

  r2. c4. <c e g>4. |
  c8^\markup{\mytime 11 70 4} (e g)   g(e c)   c4. r4. |

  r2. d4. <d fis a>4. |
  d8_\markup{\tiny {These notes may be difficult, avoid forcing your voice!}} (fis a) a(fis d) d4. r4. \bar "|."


}
\new Lyrics \lyricmode {
  "A2"1. "-"1. "B2"1. "-"1. "C3"1. "-"1. "C3"1. "-"1. "D3"1. "-"1.
  "E3"1. "-"1. "F3"1. "-"1. "G3"1. "-"1. "A3"1. "-"1. "B3"1. "-"1.
  "C4"1. "-"1. "C4"1. "-"1. "D4"1. "-"1.
}
>>

% Local Variables:
% LilyPond-master-file: "../scores/major-triad.ly"
% End:
