%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/
 
<<
\new Staff \relative {
  \clef "treble_8"
  
  c4 cis d dis e f fis g gis a ais b |
  c4 cis d dis e f fis g gis a ais b |
  c4 cis \bar "|."    
}
\addlyrics {  
  "C3" "C#3" "D3" "D#3" "E3" "F3" "F#3" "G3" "G#3" "A3" "A#3" "B3"
  "C4" "C#4" "D4" "D#4" "E4" "F4" "F#4" "G4" "G#4" "A4" "A#4" "B4"
  "C5" "C#5"
}
\addlyrics {
  %% C3 to B3
  "130.81" "138.59" "146.83" "155.56" "164.81" "174.61" "185.00" "196.00" "207.65" "220.00" "233.08" "246.94"
  %% C4 to B4
  "261.63" "277.18" "293.66" "311.13" "329.63" "349.23" "369.99" "392.00" "415.30" "440.00" "466.16" "493.88"
  %% C5 to B5
  "523.25" "554.37"
}
>>

  
% Local Variables:
% LilyPond-master-file: "../scores/chromatic-reference.ly"
% End:
