%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

#(export tempo)
#(export fourth-notes-per-time)

date=#(string-append "Date: " (strftime "%d/%m/%Y" (localtime (current-time))))

%% \displayScheme \markup { \tiny 8 "0:36" }
%% %% Results: (markup #:line (#:tiny "0:36"))

#(define fourth-notes-per-time 6)
#(define my-tempo 80)

#(define (set-tempo new-tempo)
  (set! my-tempo new-tempo))

#(define (time-number->seconds time-number tempo fnpt)
  (* fnpt ;; notes per time signature (FNpT is fourt-notes-per-time)
   (/ 60 tempo) ;; one minute / tempo (one note 4 duration)
   time-number))

#(define (time-number->time-string time-number tempo fnpt)
  (let ([seconds (time-number->seconds time-number tempo fnpt)])
   (format #f "~2,'0d:~2,'0d"
    (floor (/ seconds 60))
    (remainder (round seconds) 60))))
  
mytime = #(define-markup-command (mytime layout props time-number tempo fnpt) (markup? markup? markup?)
           (interpret-markup layout props
            (markup #:line             
             (#:tiny (time-number->time-string (string->number time-number)
                                               (string->number tempo)
                                               (string->number fnpt))))))

%% \displayScheme \markup{ \mytime 8 }
