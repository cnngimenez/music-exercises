%% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
%% License information: http://creativecommons.org/licenses/by-sa/4.0/

mytitle = #(define-markup-command (mytitle layout props text) (markup?)
            (interpret-markup layout props
             (markup
              #:line
              (#:column
               (#:null
                #:null
                #:fill-line
                (#:center-align (#:fontsize 5 text)))))))
